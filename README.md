
Based on [NgAdmin](http://ng-admin-book.marmelab.com)

## Running the app

After you have installed all dependencies you can now run the app with:
```bash
npm start
```
