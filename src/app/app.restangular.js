function requestInterceptor(RestangularProvider) {
    // use the custom query parameters function to format the API request correctly
    RestangularProvider.addFullRequestInterceptor(function(element, operation, what, url, headers, params) {
        if (operation == "getList") {
            // ** Attrs **
            // NgAdmin: ?_page=1&_perPage=30&_sortDir=DESC&_sortField=id

            // SailsJs: http://sailsjs.org/documentation/reference/blueprint-api/find-where
            // ?skip=30, ?limit=100, ?sort=lastName%20ASC

            // custom pagination params
            if (params._page) {
                params.skip = (params._page - 1) * params._perPage;
                params.limit = params._perPage;
            }
            delete params._page;
            delete params._perPage;

            // custom sort params
            if (params._sortField) {
                params.sort = params._sortField+' '+params._sortDir;
                delete params._sortField;
                delete params._sortDir;
            }

            //where={}

            // custom filters
            if (params._filters) {
                if(params._filters['ids'] && params._filters['ids'].length) {
                    params['where'] = { id : params._filters['ids'] }
                }
                //console.error('SOME SHIT HAPPEND', params._filters);
                // for (var filter in params._filters) {
                //     params[filter] = params._filters[filter];
                // }
                delete params._filters;
            }
        }
        return { params: params };
    });
}

function responseInterceptor(RestangularProvider) {
    RestangularProvider.addResponseInterceptor(function(data, operation, what, url, response) {
        // if (operation == "getList") {
        //     var contentRange = response.headers('Content-Range');
        //     response.totalCount = contentRange.split('/')[1];
        // }
        return data;
    });
}

export default { requestInterceptor, responseInterceptor }
