export default function (nga) {

    var model = nga.entity('personType').label('Типы персоны');

    model.listView()
      .title('Типы персоны')
      .fields([
        nga.field('name')
          .label('Название')
      ])
      .listActions(['edit', 'delete']);

    model.creationView()
      .title('Создание типа персоны')
      .fields([
        nga.field('name')
          .label('Название')
      ]);
    // use the same fields for the editionView as for the creationView
    model.editionView()
      .title('Редактирование типа персоны «{{ entry.values.name }}»')
      .fields(model.creationView().fields());

    return model;
}
