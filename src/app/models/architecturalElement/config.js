export default function (nga) {

    var model = nga.entity('architecturalElement').label('Элементы и конструкции');

    model.listView()
      .title('Архитектурные элементы и конструкции')
      .fields([
          nga.field('name')
            .label('Название')
      ])
      .listActions(['edit', 'delete']);

    model.creationView()
      .title('Создание элемента')
      .fields([
          nga.field('name')
            .label('Название')
      ]);
    // use the same fields for the editionView as for the creationView
    model.editionView()
      .title('Редактирование элемента «{{ entry.values.name }}»')
      .fields(model.creationView().fields());

    return model;
}
