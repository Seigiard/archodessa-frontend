export default function (nga) {

    var model = nga.entity('architecturalObject').label('Объекты');

    model.listView()
      .title('Архитектурные объекты')
      .description('Список архитектурных объектов, т.е. домов Одессы')
      .fields([
        nga.field('name')
          .label('Название'),
        nga.field('address')
          .label('Адрес')
      ])
      .listActions(['edit', 'delete']);

    model.creationView()
      .title('Создание объекта')
      .fields([
        nga.field('name')
          .label('Название')
          .validation({ required: true }),
        nga.field('address')
          .label('Адрес'),
        nga.field('address_old')
          .label('Адрес прежний'),

        nga.field('author', 'reference')
          .label('Автор')
          .targetEntity(nga.entity('person'))
          .targetField(nga.field('name'))
          .sortField('name')
          .sortDir('ASC')
          .map(function truncate(value, entry) {
            return entry['author.id'];
          })
          .singleApiCall(ids => ({'ids': ids })),

        nga.field('coordinates')
          .label('Координаты'),
        nga.field('type', 'reference')
          .label('Тип')
          .targetEntity(nga.entity('architecturalType'))
          .targetField(nga.field('name'))
          .sortField('name')
          .sortDir('ASC')
          .map(function truncate(value, entry) {
            return entry['type.id'];
          })
          .singleApiCall(ids => ({'ids': ids })),
          // .remoteComplete(true, {
          //     refreshDelay: 200,
          //     searchQuery: search => ({ q: search })
          // }),
        nga.field('levels')
          .label('Этажность'),
        nga.field('styles', 'reference_many')
          .label('Стили')
          .targetEntity(nga.entity('architecturalStyle'))
          .targetField(nga.field('name'))
          .sortField('name')
          .sortDir('ASC')
          .map(function truncate(value, entry) {
            return value.map(v => (v.id));
          })
          .singleApiCall(ids => ({'ids': ids })),
          // .remoteComplete(true, {
          //     refreshDelay: 200,
          //     searchQuery: search => ({ q: search })
          // }),
        nga.field('elements', 'reference_many')
          .label('Элементы')
          .targetEntity(nga.entity('architecturalElement'))
          .targetField(nga.field('name'))
          .sortField('name')
          .sortDir('ASC')
          .map(function truncate(value, entry) {
            return value.map(v => (v.id));
          })
          .singleApiCall(ids => ({'ids': ids }))
          // .remoteComplete(true, {
          //     refreshDelay: 200,
          //     searchQuery: search => ({ q: search })
          // })
      ]);

    model.editionView()
      .title('Редактирование объекта «{{ entry.values.name }}»')
      .fields(model.creationView().fields());

    return model;
}
