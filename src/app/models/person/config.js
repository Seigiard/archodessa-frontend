export default function (nga) {

    var model = nga.entity('person').label('Персоны');

    model.listView()
      .title('Персоны')
      .fields([
        nga.field('name')
          .label('Имя'),
        nga.field('person_type', 'reference_many')
          .targetEntity(nga.entity('personType'))
          .targetField(nga.field('name'))
          .label('Типы')
      ])
      .listActions(['edit', 'delete']);

    model.creationView()
      .title('Создание персоны')
      .fields([
        nga.field('name')
          .label('Имя')
          .validation({ required: true }),
        nga.field('person_type', 'reference_many')
          .label('Тип')
          .targetEntity(nga.entity('personType'))
          .targetField(nga.field('name'))
          .sortField('name')
          .sortDir('ASC')
          .singleApiCall(ids => ({'ids': ids })),
          // .remoteComplete(true, {
          //     refreshDelay: 200,
          //     searchQuery: search => ({ q: search })
          // }),
      ]);

    model.editionView()
      .title('Редактирование персоны «{{ entry.values.name }}»')
      .fields(model.creationView().fields());

    return model;
}
