import "ng-admin/build/ng-admin.min.js";
import "ng-admin/build/ng-admin.min.css";

// import config from 'app.config';

const MODULE_NAME = 'app';

var app = angular.module(MODULE_NAME, ['ng-admin']);

import restangularConfig from './app.restangular';
import translation from './app.translation';
import architecturalStyleModel from './models/architecturalStyle/config';
import architecturalElementModel from './models/architecturalElement/config';
import architecturalTypeModel from './models/architecturalType/config';
import architecturalObjectModel from './models/architecturalObject/config';
import personTypeModel from './models/personType/config';
import personModel from './models/person/config';


app.config(['$translateProvider', translation]);
app.config(['RestangularProvider', restangularConfig.requestInterceptor]);
app.config(['RestangularProvider', restangularConfig.responseInterceptor]);

app.config(['NgAdminConfigurationProvider', function (nga) {
    // create an admin application
    var admin = nga.application()
        .title('Архитектура Одессы')
        //.debug(false)
        .baseApiUrl('http://localhost:1337/'); // main API endpoint


    var architecturalStyle = architecturalStyleModel(nga);
    var architecturalElement = architecturalElementModel(nga);
    var architecturalType = architecturalTypeModel(nga);
    var architecturalObject = architecturalObjectModel(nga);
    var personType = personTypeModel(nga);
    var person = personModel(nga);

    admin.addEntity(architecturalObject);
    admin.addEntity(architecturalStyle);
    admin.addEntity(architecturalElement);
    admin.addEntity(architecturalType);
    admin.addEntity(person);
    admin.addEntity(personType);

    admin.menu(
      nga.menu()
        .addChild(nga.menu(admin.getEntity('architecturalObject'))
          .icon('<span class="fa fa-building-o fa-fw"></span>'))
        .addChild(
          nga.menu()
            .title('Системные')
            .icon('<span class="fa fa-cog fa-fw"></span>')
            .addChild(nga.menu(admin.getEntity('architecturalStyle')))
            .addChild(nga.menu(admin.getEntity('architecturalElement')))
            .addChild(nga.menu(admin.getEntity('architecturalType')))
            .addChild(nga.menu(admin.getEntity('person')))
            .addChild(nga.menu(admin.getEntity('personType')))
        )
    );

    nga.configure(admin);
}]);

export default MODULE_NAME;
